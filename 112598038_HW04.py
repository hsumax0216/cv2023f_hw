import cv2
import numpy as np
import math
import os
import time

def RGB2Grayscale(src):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        Gray = 0.299 * R + 0.587 * G + 0.114 * B
        Gray = Gray.astype(np.uint8)
        return Gray

def zeropadding(src,pad):
    if len(src.shape) == 2:
        imgH,imgW = src.shape
        result = np.zeros((imgH+2*pad,imgW+2*pad))
        result[pad:pad+imgH,pad:pad+imgW] = src
        return result

def twoDimSum(src):
    if len(src.shape) == 2:
        sum = 0
        imgH,imgW = src.shape
        for i in range(imgH):
            for j in range(imgW):
                sum += src[i,j]
        return sum

def Conv2d(src,ker,strides=1):    
    if len(src.shape) == 2:
        # one channel convolution
        B = src
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        zB = zeropadding(B,padding)
        for i in range(0,paddedH,strides):
            for j in range(0,paddedW,strides):
                oB [i,j] = twoDimSum(zB[i:i+kerH,j:j+kerW]*ker)
        return oB
    if len(src.shape) == 3:
        # three channels convolution
        B, G, R = cv2.split(src)
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        oG = np.zeros((paddedH,paddedW))
        oR = np.zeros((paddedH,paddedW))
        zB = zeropadding(B,padding)
        zG = zeropadding(G,padding)
        zR = zeropadding(R,padding)
        for i in range(0,paddedH,strides):
            for j in range(0,paddedW,strides):
                oB [i,j] = twoDimSum(zB[i:i+kerH,j:j+kerW]*ker)
                oG [i,j] = twoDimSum(zG[i:i+kerH,j:j+kerW]*ker)
                oR [i,j] = twoDimSum(zR[i:i+kerH,j:j+kerW]*ker)
        return cv2.merge((oB, oG, oR))

def gaussianKernelMaker(sigma,size):
    if(size%2==0):
        print('size is even!')
        return None
    kernel = np.zeros((size,size),dtype=np.float32)
    halfsize = size // 2
    sigmasqtwo = sigma * sigma * 2
    reducesum = 0
    for i in range(0,size):
        Xsq=(i-halfsize)*(i-halfsize)
        for j in range(0,size):
            Ysq=(j-halfsize)*(j-halfsize)
            kernel[i][j] = math.exp((-1)*(Xsq+Ysq)/(sigmasqtwo))
            reducesum += kernel[i][j]
    kernel = kernel / reducesum
    return kernel

def generateInitialSnake(image, center = None, radius=-1, radiusRatio=3, n_points=50):
    rows, cols = image.shape[:2]
    if radius < 0:
        radius = rows  if rows < cols else cols
        radius = radius // 2
        radius = radius - radius // radiusRatio
    if center is None:
        center = (cols // 2, rows // 2)
    theta = np.linspace(0, 2 * np.pi, n_points)
    x = center[0] + radius * np.cos(theta)
    y = center[1] + radius * np.sin(theta)
    return np.array([y, x]).T

def linalgNormPow(points):
    points = np.array(points)
    if len(points.shape) == 1:
        return np.array([points[0]**2+points[1]**2]).astype(points.dtype)
    normPow = []
    for x,y in points:
        normPow.append(x**2+y**2)
    return np.array(normPow).astype(points.dtype)

def ActiveContour(src, points, alpha, beta, gamma, search_region_size):
    """
    Set a search region:
    For each pixel in search region:
        (a) Calculate Ecount,Ecurv,Eimg
        (b) Etotal = a*Ecount + b*Ecurv + y*Eimg
        (c) if Etotal < Emin:
                (1) Update Emin
                (2) Update point position
    Return points
    """
    if len(src.shape) == 2:
        srcH, srcW = src.shape        
        Points = np.copy(points).astype(np.int32)
        for i,point in enumerate(Points):
            y, x = np.int32(point)
            E_min = np.inf
            best_point = point

            shift_pPre = np.int32(Points[(i-1)%len(Points)])
            shift_pNxt = np.int32(Points[(i+1)%len(Points)])
            # 為每個點設定搜尋區域
            for dy in range(-search_region_size, search_region_size + 1):
                for dx in range(-search_region_size, search_region_size + 1):
                    ny, nx = y + dy, x + dx
                    # 確保點在影像範圍內
                    if 0 <= ny < srcH and 0 <= nx < srcW:
                        p = np.array([ny, nx])
                        E_cont = linalgNormPow(p - shift_pPre)
                        E_curv = linalgNormPow(shift_pPre - 2*p + shift_pNxt)
                        E_img = -src[ny,nx]
                        E_total = alpha * E_cont + beta * E_curv + gamma * E_img
                        # 更新最小能量和點位置
                        if E_total < E_min:
                            E_min = E_total
                            best_point = [ny, nx]

            Points[i] = best_point
        return Points

def drawLineOnImg(src,points,lColor=(255,0,0),lThickness = 1,cColor=(0,0,255),cThickness = -1,cRadius = 3):
    if len(src.shape) == 3 and len(points) > 0:
        # print('In drawLineOnImg.')
        yo, xo = points[0]
        yo, xo = int(yo), int(xo)
        yp, xp = yo, xo
        for num in range(1,len(points)):
            yc, xc = points[num]
            yc, xc = int(yc), int(xc)
            cv2.line(src,(xp,yp),(xc,yc),lColor,lThickness)
            cv2.circle(src, (xc,yc), cRadius, cColor, cThickness)
            xp, yp = xc, yc        
        cv2.line(src,(xp,yp),(xo,yo),lColor,lThickness)
        cv2.circle(src, (xo, yo), cRadius, cColor, cThickness)
        return src

def seconds_to_minutes_and_hours(seconds):
    minutes = seconds // 60
    hours = minutes // 60
    return hours, minutes%60, seconds%60

def FindTheContour(src,
                   blurred = None,
                   gausSize = 1,
                   sigma = 1,
                   alpha = 0.3,
                   beta = 0.01,
                   gamma = 10,
                   search_region_size = 5,
                   snake = None,
                   MAX_ITERATION = 80,
                   vidpath = 'vid.mp4'):
    if src.shape[2] == 3:
        PI = 3.14159
        output_fps = 10
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        output_video_path = os.path.join(vidpath)
        target_sizeH,target_sizeW,_ = src.shape
        out = cv2.VideoWriter(output_video_path, fourcc, output_fps, (target_sizeH,target_sizeW))
        sobelx = np.array([[ -1,  0,  1],
                            [ -2,  0,  2],
                            [ -1,  0,  1]])
        sobely = np.array([[ -1,  -2,  -1],
                            [  0,  0,  0],
                            [  1,  2,  1]])

        if blurred is None:
            gray = RGB2Grayscale(src)
            gausKer = gaussianKernelMaker(sigma,gausSize)
            blurred = Conv2d(gray,gausKer)

        if snake is None:
            snake = generateInitialSnake(blurred)

        gradientx = Conv2d(blurred,sobelx)
        gradienty = Conv2d(blurred,sobely)        
        magnitude = np.sqrt(gradienty**2 + gradientx**2)
        magnitude = magnitude.astype(np.float32)
        
        points = np.copy(snake).astype(np.int32)
        result_frame = np.copy(src).astype(np.uint8)
        result_frame = drawLineOnImg(result_frame,points)

        starttime = time.time()

        newPoints = None
        for i in range(MAX_ITERATION):
            result_frame_copy = np.copy(result_frame).astype(np.uint8)
            newPoints = ActiveContour(magnitude, points, alpha, beta, gamma, search_region_size)
            if np.all(newPoints == points):
                print(f'points is not changing, Break at i: {i}')
                result_frame_copy = drawLineOnImg(result_frame_copy,newPoints)
                out.write(result_frame_copy)
                break
            points = newPoints
            result_frame_copy = drawLineOnImg(result_frame_copy,points)
            out.write(result_frame_copy)
        
        endtime = time.time()
        exec_time = endtime - starttime
        hours, minutes, seconds = seconds_to_minutes_and_hours(exec_time)
        print(f"執行時間: {hours}小時 {minutes}分鐘 {seconds:.2f}秒鐘")

        out.release()
        print(f"Output video saved at {output_video_path}")
        return result_frame_copy

print("Start program.")
img1 = cv2.imread('./test_img/img1.jpg')
img2 = cv2.imread('./test_img/img2.jpg')
img3 = cv2.imread('./test_img/img3.jpg')

if img1 is None or img2 is None or img3 is None :
    print('Image can\'t read')
    exit(0)

gaussianKer1 = gaussianKernelMaker(3,5)

gauss1 = Conv2d(RGB2Grayscale(img1),gaussianKer1)
gauss2 = Conv2d(RGB2Grayscale(img2),gaussianKer1)
gauss3 = Conv2d(RGB2Grayscale(img3),gaussianKer1)

contour1 = FindTheContour(img1,gauss1,
                          alpha = 0.3,
                          beta = 0.01,
                          gamma = 10,
                          MAX_ITERATION = 45,
                          vidpath = './result_img/vid_img1.mp4')
cv2.imwrite('./result_img/result_img1.jpg', contour1)
print("Q1 finished.")
contour2 = FindTheContour(img2,gauss2,
                          snake = generateInitialSnake(gauss2,radiusRatio=7,radius=350,n_points=50),
                          alpha = 0.05,
                          beta = 0.1,
                          gamma = 5,
                        #   search_region_size = 15,
                          MAX_ITERATION = 60,
                          vidpath = './result_img/vid_img2.mp4')
cv2.imwrite('./result_img/result_img2.jpg', contour2)
print("Q2 finished.")

contour3 = FindTheContour(img3,gauss3,
                          snake = generateInitialSnake(gauss3,center=(490,585),radius=270,n_points=50),
                          alpha = 0.1,
                          beta = 0.4,
                          gamma = 2.0,
                        #   search_region_size = 10,
                        #   MAX_ITERATION = 40,
                          vidpath = './result_img/vid_img3.mp4')
cv2.imwrite('./result_img/result_img3.jpg', contour3)
print("Q3 finished.")

print("End program.")
