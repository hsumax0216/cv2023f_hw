import cv2
import numpy as np
import matplotlib.pyplot as plt

def zeropadding(src,pad):
    if len(src.shape) == 2:
        imgH,imgW = src.shape
        result = np.zeros((imgH+2*pad,imgW+2*pad))
        result[pad:pad+imgH,pad:pad+imgW] = src
        return result

def Conv2d(src,ker,strides=1):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        oG = np.zeros((paddedH,paddedW))
        oR = np.zeros((paddedH,paddedW))
        zB = zeropadding(B,padding)
        zG = zeropadding(G,padding)
        zR = zeropadding(R,padding)
        for i in range(0,paddedH,strides):
            for j in range(0,paddedW,strides):
                oB [i,j] = np.clip(np.sum(zB[i:i+kerH,j:j+kerW]*ker),0,255)
                oG [i,j] = np.clip(np.sum(zG[i:i+kerH,j:j+kerW]*ker),0,255)
                oR [i,j] = np.clip(np.sum(zR[i:i+kerH,j:j+kerW]*ker),0,255)
        return cv2.merge((oB, oG, oR))
                 
def MedianfilterConv2d(src,kerHWSize=3,strides=1):
    if kerHWSize%2==1 and kerHWSize>=3 and src.shape[2] == 3:
        ker = np.zeros((kerHWSize,kerHWSize))
        kerCenter = kerHWSize//2
        ker[kerCenter,kerCenter] = 1  
        B, G, R = cv2.split(src)
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        oG = np.zeros((paddedH,paddedW))
        oR = np.zeros((paddedH,paddedW))
        zB = zeropadding(B,padding)
        zG = zeropadding(G,padding)
        zR = zeropadding(R,padding)
        medIdx = kerH*kerW//2
        for i in range(0,paddedH,strides):
            for j in range(0,paddedW,strides):
                boxB = []
                boxG = []
                boxR = []
                for ki in range(0,kerH):
                    for kj in range(0,kerW):
                        boxB.append(zB[i+ki,j+kj])
                        boxG.append(zG[i+ki,j+kj])
                        boxR.append(zR[i+ki,j+kj])
                boxB.sort()
                boxG.sort()
                boxR.sort()
                oB [i,j] = boxB[medIdx]
                oG [i,j] = boxG[medIdx]
                oR [i,j] = boxR[medIdx]
        return cv2.merge((oB, oG, oR))

def ImgHistogram(src,imgPath,imgTitle):
    if len(src.shape) == 2:
        labelName = imgPath.split('/')[-1].split('.')[0]

        data=np.zeros(256)
        for i in range(src.shape[0]):
            for j in range(src.shape[1]):
                data[int(src[i,j])] += 1

        plt.figure(figsize=(15, 7), dpi=300)
        xIdx = np.arange(256)
        plt.bar(xIdx,data, alpha=0.7,label=labelName)

        plt.title(imgTitle)
        plt.xlabel('pixel value')
        plt.ylabel('counts')
        plt.xticks(np.arange(0, 257, 16))
        plt.legend()

        plt.savefig(imgPath)

print("Start program.")
img1 = cv2.imread('./test_img/noise1.png')
img2 = cv2.imread('./test_img/noise2.png')

if img1 is None or img2 is None:
    print('Image can\'t read')
    exit(0)

Meanfilter = np.array([[ 1,  1,  1],
                        [ 1,  1,  1],
                        [ 1,  1,  1]])/9

Mean1 = Conv2d(img1,Meanfilter)[:,:,0]
cv2.imwrite('./result_img/noise1_q1.png', Mean1)
Mean2 = Conv2d(img2,Meanfilter)[:,:,0]
cv2.imwrite('./result_img/noise2_q1.png', Mean2)
print("Q1 finished.")

Median1 = MedianfilterConv2d(img1)[:,:,0]
cv2.imwrite('./result_img/noise1_q2.png', Median1)
Median2 = MedianfilterConv2d(img2)[:,:,0]
cv2.imwrite('./result_img/noise2_q2.png', Median2)
print("Q2 finished.")

ImgHistogram(img1[:,:,0],'./result_img/noise1_his.png','Histrogram - noise1')
ImgHistogram(img2[:,:,0],'./result_img/noise2_his.png','Histrogram - noise2')

ImgHistogram(Mean1,'./result_img/noise1_q1_his.png','Histrogram - noise1_Q1')
ImgHistogram(Mean2,'./result_img/noise2_q1_his.png','Histrogram - noise2_Q1')

ImgHistogram(Median1,'./result_img/noise1_q2_his.png','Histrogram - noise1_Q2')
ImgHistogram(Median2,'./result_img/noise2_q2_his.png','Histrogram - noise2_Q2')
print("Q3 finished.")

print("End program.")
