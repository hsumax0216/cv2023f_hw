def quick_sort(recurArr,compare_func = None):
    if len(recurArr) < 2:
        return recurArr
    else:
        med = len(recurArr)//2
        pivot = recurArr[med]
        min = recurArr[0]
        max = recurArr[-1]
        if not compare_func(min,max):
            min,max = max,min
        if not compare_func(pivot,max):
            recurArr[0],recurArr[-1] = recurArr[-1],recurArr[0]
        elif compare_func(min,pivot):
            recurArr[0],recurArr[med] = recurArr[med],recurArr[0]
        
        pivot = recurArr[0]
        less = []
        for x in recurArr[1:]:
            if compare_func(x,pivot):
                less.append(x)
        greater = []
        for x in recurArr[1:]:
            if not compare_func(x,pivot):
                greater.append(x)

        return quick_sort(less, compare_func) + [pivot] + quick_sort(greater, compare_func)
    
# 比較函式的例子：按照升序排列
def compare_ascending(x, y):
    return x <= y

# 比較函式的例子：按照降序排列
def compare_descending(x, y):
    return x >= y

# 範例使用
arr = [3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5]

# 使用升序比較函式
sorted_arr_ascending = quick_sort(arr, compare_ascending)
print("Ascending Order:", sorted_arr_ascending)

# 使用降序比較函式
sorted_arr_descending = quick_sort(arr, compare_descending)
print("Descending Order:", sorted_arr_descending)