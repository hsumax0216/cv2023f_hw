import cv2
import numpy as np

def RGB2Grayscale(src):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        Gray = 0.299 * R + 0.587 * G + 0.114 * B
        Gray = Gray.astype(np.uint8)
        return cv2.merge((Gray, Gray, Gray))

def zeropadding(src,pad):
    if len(src.shape) == 2:
        imgH,imgW = src.shape
        result = np.zeros((imgH+2*pad,imgW+2*pad))
        result[pad:pad+imgH,pad:pad+imgW] = src
        return result

def Conv2d(src,ker,strides=1):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        oG = np.zeros((paddedH,paddedW))
        oR = np.zeros((paddedH,paddedW))
        pB = zeropadding(B,padding)
        pG = zeropadding(G,padding)
        pR = zeropadding(R,padding)
        offsetH = padding*2
        offsetW = padding*2
        for i in range(0,B.shape[0]-offsetH,strides):
            for j in range(0,B.shape[1]-offsetW,strides):
                oB [i,j] = np.clip(np.sum(pB[i+padding:i+padding+kerH,j+padding:j+padding+kerW]*ker),0,255)
                oG [i,j] = np.clip(np.sum(pG[i+padding:i+padding+kerH,j+padding:j+padding+kerW]*ker),0,255)
                oR [i,j] = np.clip(np.sum(pR[i+padding:i+padding+kerH,j+padding:j+padding+kerW]*ker),0,255)
        return cv2.merge((oB, oG, oR))

def Maxpooling(src,poolSize,strides):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        
        inputH ,inputW = B.shape
        outputH = (inputH-poolSize)//strides+1
        outputW = (inputW-poolSize)//strides+1
        oB = np.zeros((outputH,outputW))
        oG = np.zeros((outputH,outputW))
        oR = np.zeros((outputH,outputW))
        for i in range(0,inputH-poolSize+1,strides):
            for j in range(0,inputW-poolSize+1,strides):
                oB[i//strides,j//strides] = np.max(B[i:i+poolSize,j:j+poolSize])
                oG[i//strides,j//strides] = np.max(G[i:i+poolSize,j:j+poolSize])
                oR[i//strides,j//strides] = np.max(R[i:i+poolSize,j:j+poolSize])
        return cv2.merge((oB, oG, oR))

def binarythreshold(src,threshold):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        oB = np.zeros((B.shape[0],B.shape[1]))
        oG = np.zeros((B.shape[0],B.shape[1]))
        oR = np.zeros((B.shape[0],B.shape[1]))
        for i in range(B.shape[0]):
            for j in range(B.shape[1]):
                oB[i][j] = 0 if (B[i][j] >= threshold) else 255
                oG[i][j] = 0 if (G[i][j] >= threshold) else 255
                oR[i][j] = 0 if (R[i][j] >= threshold) else 255
        return cv2.merge((oB, oG, oR))
                



img1 = cv2.imread('./test_img/aeroplane.png')
img2 = cv2.imread('./test_img/taipei101.png')

if img1 is None or img2 is None:
    print('img cant read')
    exit(0)

Grayout1 = RGB2Grayscale(img1)
cv2.imwrite('./result_img/aeroplane_Q1.png', Grayout1)
Grayout2 = RGB2Grayscale(img2)
cv2.imwrite('./result_img/taipei101_Q1.png', Grayout2)

edgeDetectKernel = np.array([[-1, -1, -1],
                            [-1,  8, -1],
                            [-1, -1, -1]])

edgeDet1 = Conv2d(Grayout1,edgeDetectKernel)
cv2.imwrite('./result_img/aeroplane_Q2.png', edgeDet1)
edgeDet2 = Conv2d(Grayout2,edgeDetectKernel)
cv2.imwrite('./result_img/taipei101_Q2.png', edgeDet2)

max1 = Maxpooling(edgeDet1,2,2)
cv2.imwrite('./result_img/aeroplane_Q3.png', max1)
max2 = Maxpooling(edgeDet2,2,2)
cv2.imwrite('./result_img/taipei101_Q3.png', max2)

threshold =128
binary1 = binarythreshold(max1,threshold)
cv2.imwrite('./result_img/aeroplane_Q4.png', binary1)
binary2 = binarythreshold(max2,threshold)
cv2.imwrite('./result_img/taipei101_Q4.png', binary2)


