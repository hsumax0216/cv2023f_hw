
import math
import numpy as np
import matplotlib.pyplot as plt
import cv2

img = cv2.imread('img1.png')
src = img.copy()
image_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
print("img imshow:")
plt.imshow(image_rgb)
plt.show()
img = cv2.Canny(img, 50, 150)
image_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
print("Canny imshow:")
plt.imshow(image_rgb)
plt.show()
print(img.shape)

R = int(math.sqrt(img.shape[0]**2 + img.shape[1]**2))
theta_values = np.arange(0, 2*np.pi, np.pi/180)  # 角度
T = theta_values.shape[0]
counter = np.zeros((2*R,T))

edge_pixels = np.argwhere(img > 0)

calculates = 0

for y, x in edge_pixels:
    for theta_index, theta in enumerate(theta_values):
        rho = int(x * np.cos(theta) + y * np.sin(theta))
        counter[rho , theta_index] += 1
        calculates += 1

print("calculates: ",calculates)
threshold = 50

peaks = np.argwhere(counter > threshold)
peak_values = [counter[peak[0], peak[1]] for peak in peaks]
sorted_peaks = np.argsort(peak_values)[::-1]
sorted_peaks = sorted_peaks[:min(len(sorted_peaks), 30)]  # 只取前10个峰值
peaks = [(peaks[i, 0], theta_values[peaks[i, 1]]) for i in sorted_peaks]

print(len(peaks))
print(peaks)
print(img.shape)
print(counter.shape)

image_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

print("counter imshow:")
plt.imshow(counter)
plt.show()

height, width = img.shape
image_with_lines = np.copy(src).astype(np.uint8)
image_with_lines_plt = np.copy(image_with_lines)
image_with_lines_plt = cv2.cvtColor(image_with_lines_plt, cv2.COLOR_BGR2RGB)

print("image_with_lines_plt imshow:")
plt.imshow(image_with_lines_plt)
for rho, theta in peaks:

    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a * rho
    y0 = b * rho
    x1 = int(x0 + R * (-b))
    y1 = int(y0 + R * (a))
    x2 = int(x0 - R * (-b))
    y2 = int(y0 - R * (a))

    plt.plot([x1, x2], [y1, y2], 'r-')
    cv2.line(image_with_lines, (x1, y1), (x2, y2), (0, 0, 255), 2)
plt.show()

image_with_lines = cv2.cvtColor(image_with_lines, cv2.COLOR_BGR2RGB)
print("image_with_lines imshow:")
plt.imshow(image_with_lines)
plt.show()
