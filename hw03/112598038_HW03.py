import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

def RGB2Grayscale(src):
    if src.shape[2] == 3:
        B, G, R = cv2.split(src)
        Gray = 0.299 * R + 0.587 * G + 0.114 * B
        Gray = Gray.astype(np.uint8)
        return Gray

def zeropadding(src,pad):
    if len(src.shape) == 2:
        imgH,imgW = src.shape
        result = np.zeros((imgH+2*pad,imgW+2*pad))
        result[pad:pad+imgH,pad:pad+imgW] = src
        return result

def twoDimSum(src):
    if len(src.shape) == 2:
        sum = 0
        imgH,imgW = src.shape
        for i in range(imgH):
            for j in range(imgW):
                sum += src[i,j]
        return sum

def Conv2d(src,ker,strides=1):    
    if len(src.shape) == 2:
        # one channel convolution
        B = src
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        zB = zeropadding(B,padding)
        for i in range(0,paddedH,strides):
            for j in range(0,paddedW,strides):
                # 不使用clipping,clipping會造成太多資訊被卷積刪掉
                # Without clipping, clipping will cause too much information to be deleted by convolution.
                # oB [i,j] = onePixelClip(twoDimSum(zB[i:i+kerH,j:j+kerW]*ker),0,255)
                oB [i,j] = twoDimSum(zB[i:i+kerH,j:j+kerW]*ker)
        return oB
    if len(src.shape) == 3:
        # three channels convolution
        B, G, R = cv2.split(src)
        kerH,kerW = ker.shape
        padding = kerH//2
        paddedH = (B.shape[0]+2*padding-kerH)//strides+1
        paddedW = (B.shape[1]+2*padding-kerW)//strides+1
        oB = np.zeros((paddedH,paddedW))
        oG = np.zeros((paddedH,paddedW))
        oR = np.zeros((paddedH,paddedW))
        zB = zeropadding(B,padding)
        zG = zeropadding(G,padding)
        zR = zeropadding(R,padding)
        for i in range(0,paddedH,strides):
            for j in range(0,paddedW,strides):
                # 不使用clipping,clipping會造成太多資訊被卷積刪掉
                # Without clipping, clipping will cause too much information to be deleted by convolution.
                # oB [i,j] = onePixelClip(twoDimSum(zB[i:i+kerH,j:j+kerW]*ker),0,255)
                # oG [i,j] = onePixelClip(twoDimSum(zG[i:i+kerH,j:j+kerW]*ker),0,255)
                # oR [i,j] = onePixelClip(twoDimSum(zR[i:i+kerH,j:j+kerW]*ker),0,255)
                oB [i,j] = twoDimSum(zB[i:i+kerH,j:j+kerW]*ker)
                oG [i,j] = twoDimSum(zG[i:i+kerH,j:j+kerW]*ker)
                oR [i,j] = twoDimSum(zR[i:i+kerH,j:j+kerW]*ker)
        return cv2.merge((oB, oG, oR))

def quick_sort(recurArr,compare_func = None):
    if len(recurArr) < 2:
        return recurArr
    else:
        med = len(recurArr)//2
        pivot = recurArr[med]
        min = recurArr[0]
        max = recurArr[-1]
        if not compare_func(min,max):
            min,max = max,min
        if not compare_func(pivot,max):
            recurArr[0],recurArr[-1] = recurArr[-1],recurArr[0]
        elif compare_func(min,pivot):
            recurArr[0],recurArr[med] = recurArr[med],recurArr[0]
        
        pivot = recurArr[0]
        less = []
        for x in recurArr[1:]:
            if compare_func(x,pivot):
                less.append(x)
        greater = []
        for x in recurArr[1:]:
            if not compare_func(x,pivot):
                greater.append(x)

        return quick_sort(less, compare_func) + [pivot] + quick_sort(greater, compare_func)

def compare_ascending(x, y):
    return x <= y

def compare_descending(x, y):
    return x >= y

def gaussianKernelMaker(sigma,size):
    if(size%2==0):
        print('size is even!')
        return None
    # PI = 3.14159
    kernel = np.zeros((size,size),dtype=np.float32)
    halfsize = size // 2
    sigmasqtwo = sigma * sigma * 2
    reducesum = 0
    for i in range(0,size):
        Xsq=(i-halfsize)*(i-halfsize)
        for j in range(0,size):
            Ysq=(j-halfsize)*(j-halfsize)
            kernel[i][j] = math.exp((-1)*(Xsq+Ysq)/(sigmasqtwo))
            # kernel[i][j] = 1/(PI * sigmasqtwo) * math.exp((-1)*(Xsq+Ysq)/(sigmasqtwo))
            reducesum += kernel[i][j]
    # return kernel / (PI * sigmasqtwo)
    kernel = kernel / reducesum
    return kernel

def CannyEdgeDetect(src,low_thres,up_thres,gausSize = 1,sigma = 1,blur = None):
    if src.shape[2] == 3:
        PI = 3.14159
        sobelx = np.array([[ -1,  0,  1],
                            [ -2,  0,  2],
                            [ -1,  0,  1]])
        sobely = np.array([[ -1,  -2,  -1],
                            [  0,  0,  0],
                            [  1,  2,  1]])
        if blur is None:
            gausKer = gaussianKernelMaker(sigma,gausSize)
            blur = Conv2d(src,gausKer)
        if len(blur.shape) > 2:
            blur = RGB2Grayscale(blur)
        gradientx = Conv2d(blur,sobelx)
        gradienty = Conv2d(blur,sobely)
        print('sobel conv2d pass')

        magnitude = np.sqrt(gradientx**2 + gradienty**2)
        magnitude = magnitude.astype(np.float32)
        angle = np.arctan2(gradienty,gradientx) * (180/PI)
        angle = angle.astype(np.float32)
        print('magnitude &  angle pass')

        nonMaxSupperessed = np.zeros(magnitude.shape)
        nintyCount = 0
        nagCount = 0
        for i in range(1,magnitude.shape[0]-1):
            for j in range(1,magnitude.shape[1]-1):                
                anglecell = angle[i,j]

                if anglecell < 0:
                    anglecell = -anglecell

                if (0 <= anglecell < 22.5) or (157.5 <= anglecell <= 180):
                    neighbor_pixels = [magnitude[i,j-1], magnitude[i,j], magnitude[i,j+1]]
                elif (22.5 <= anglecell < 67.5):
                    neighbor_pixels = [magnitude[i+1,j-1], magnitude[i,j], magnitude[i-1,j+1]]
                elif (67.5 <= anglecell < 112.5):
                    neighbor_pixels = [magnitude[i+1,j], magnitude[i,j], magnitude[i-1,j]]
                else:
                    # 112.5 <= anglecell < 157.5
                    neighbor_pixels = [magnitude[i-1,j-1], magnitude[i,j], magnitude[i+1,j+1]]
                
                if (angle[i,j]>90):
                    nintyCount+=1

                if (angle[i,j]<0):
                    nagCount+=1

                if magnitude[i,j] >= max(neighbor_pixels):
                    nonMaxSupperessed[i,j] = magnitude[i,j]
        print(f'angle > 90 degrees: {nintyCount} times')
        print(f'angle <  0 degrees: {nagCount} times')
        print('Non Max Supperessed pass')

        edgeImg = np.zeros(magnitude.shape)

        strongx,strongy = np.where(nonMaxSupperessed >= up_thres)
        weakx,weaky = np.where((low_thres <= nonMaxSupperessed) & (nonMaxSupperessed < up_thres))
        strong = 255
        weak = 25

        edgeImg[strongx,strongy] = strong
        edgeImg[weakx,weaky] = weak
        # cv2.imwrite('./result_img/edgeImg.png', edgeImg)

        for i in range(1,edgeImg.shape[0]-4):
            for j in range(1,edgeImg.shape[1]-4):
                if edgeImg[i,j] == weak:
                    if np.max(edgeImg[i-1:i+2,j-1:j+2]) != strong:
                        edgeImg[i,j] = 0
                    else:
                        edgeImg[i,j] = strong
        print('Edge track pass')
        return edgeImg

def HTcounter_compare_descending(x, y):
    return x[0] >= y[0]

def HoughTransform(src,edge):
    if src.shape[2]==3 and len(edge.shape)==2:
        srccopy = np.copy(src).astype(np.uint8)
        maxR = int(math.sqrt(edge.shape[0]**2 + edge.shape[1]**2))
        theta_values = np.arange(0, np.pi, np.pi/180)  # angle list values
        T = theta_values.shape[0]
        counter = np.zeros((maxR,T))
        
        # i == y-axis
        for i in range(edge.shape[0]):
            # j == x-axis
            for j in range(edge.shape[1]):
                if edge[i,j] > 0:
                    for thetaidx,theta in enumerate(theta_values):
                        # rho.d
                        # rho = x*cos(theta) + y*sin(theta)
                        rho = int(j * math.cos(theta) + i * math.sin(theta))
                        counter[rho,thetaidx] += 1
        counterlist = []

        threshold = 50
        for i in range(counter.shape[0]):
            for j in range(counter.shape[1]):
                # element: counts,rho,thetaidx
                if counter[i,j] > threshold:
                    counterlist.append([counter[i,j],i,j])
        sortedcounterlist = quick_sort(counterlist,HTcounter_compare_descending)
        
        # extract top 30
        sortedcounterlist = sortedcounterlist[0:30]

        for _,rho,thetaidx in sortedcounterlist:
            Cos = math.cos(theta_values[thetaidx])
            Sin = math.sin(theta_values[thetaidx])
            xL = Cos * rho
            yL = Sin * rho
            x1 = int(xL + maxR * (-Sin))
            y1 = int(yL + maxR * (Cos))
            x2 = int(xL - maxR * (-Sin))
            y2 = int(yL - maxR * (Cos))

            cv2.line(srccopy,(x1,y1),(x2,y2),(0,0,255),2)

        return srccopy

print("Start program.")
img1 = cv2.imread('./test_img/test_img1.png')
img2 = cv2.imread('./test_img/test_img2.png')
img3 = cv2.imread('./test_img/test_img3.png')

if img1 is None or img2 is None or img3 is None :
    print('Image can\'t read')
    exit(0)

gaussianKer1 = gaussianKernelMaker(3,5)
gaussianKer2 = gaussianKernelMaker(1.2,5)

gauss1 = RGB2Grayscale(Conv2d(img1,gaussianKer1))
cv2.imwrite('./result_img/img1_q1.png', gauss1)
gauss2 = RGB2Grayscale(Conv2d(img2,gaussianKer1))
cv2.imwrite('./result_img/img2_q1.png', gauss2)
gauss3 = RGB2Grayscale(Conv2d(img3,gaussianKer2))
cv2.imwrite('./result_img/img3_q1.png', gauss3)
print("Q1 finished.")

canny1 = CannyEdgeDetect(img1,25,50,blur = gauss1)#info good
cv2.imwrite('./result_img/img1_q2.png', canny1)
canny2 = CannyEdgeDetect(img2,45,50,blur = gauss2)
cv2.imwrite('./result_img/img2_q2.png', canny2)
canny3 = CannyEdgeDetect(img3,45,50,blur = gauss3)
cv2.imwrite('./result_img/img3_q2.png', canny3)
print("Q2 finished.")

hough1 = HoughTransform(img1,canny1)
cv2.imwrite('./result_img/img1_q3.png', hough1)
hough2 = HoughTransform(img2,canny2)
cv2.imwrite('./result_img/img2_q3.png', hough2)
hough3 = HoughTransform(img3,canny3)
cv2.imwrite('./result_img/img3_q3.png', hough3)
print("Q3 finished.")

print("End program.")
